<?php

/**
 * ECSHOP 支付宝插件
 * ============================================================================
 * * 版权所有 2005-2012 上海商派网络科技有限公司，并保留所有权利。
 * 网站地址: http://www.ecshop.com；
 * ----------------------------------------------------------------------------
 * 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和
 * 使用；不允许对程序代码以任何形式任何目的的再发布。
 * ============================================================================
 * $Author: douqinghua $
 * $Id: allinpay.php 17217 2011-01-19 06:29:08Z douqinghua $
 */

if (!defined('IN_ECS'))
{
    die('Hacking attempt');
}

$payment_lang = ROOT_PATH . 'languages/' .$GLOBALS['_CFG']['lang']. '/payment/allinpay.php';

if (file_exists($payment_lang))
{
    global $_LANG;

    include_once($payment_lang);
}

/* 模块的基本信息 */
if (isset($set_modules) && $set_modules == TRUE)
{
    $i = isset($modules) ? count($modules) : 0;

    /* 代码 */
    $modules[$i]['code']    = basename(__FILE__, '.php');

    /* 描述对应的语言项 */
    $modules[$i]['desc']    = 'allinpay_desc';

    /* 是否支持货到付款 */
    $modules[$i]['is_cod']  = '0';

    /* 是否支持在线支付 */
    $modules[$i]['is_online']  = '1';

    /* 作者 */
    $modules[$i]['author']  = 'ECSHOP TEAM';

    /* 网址 */
    $modules[$i]['website'] = 'http://www.allinpay.com';

    /* 版本号 */
    $modules[$i]['version'] = '1.0.2';

    /* 配置信息 */
    $modules[$i]['config']  = array(
        array('name' => 'allinpay_pc',           'type' => 'text',   'value' => ''),
        array('name' => 'allinpay_m',           'type' => 'text',   'value' => ''),
        array('name' => 'allinpay_key',               'type' => 'text',   'value' => ''),
        array('name' => 'allinpay_pay_method',        'type' => 'select', 'value' => '')
    );

    return;
}

/**
 * 类
 */
class allinpay
{

    /**
     * 构造函数
     *
     * @access  public
     * @param
     *
     * @return void
     */

	/* 代码修改_start  By  www.68ecshop.com */
    function __construct()
    {
        $this->allinpay();
    }

	 function allinpay()
    {
    }
	/* 代码修改_end  By  www.68ecshop.com */

    /**
     * 生成支付代码
     * @param   array   $order      订单信息
     * @param   array   $payment    支付方式信息
     */
    function get_code($order, $payment)
    {
        //var_dump($order);
        //die;
        $serverUrl = "";
        $inputCharset = 1;
        $pickupUrl = return_url(basename(__FILE__, '.php'));
        $receiveUrl = return_url(basename(__FILE__, '.php'));
        $version = "v1.0";
        $language = 1;
        $signType = 1;
        $merchantId = $payment[allinpay_pc];
        $payerName = "";
        $payerEmail = "";
        $payerTelephone = "";
        $payerIDCard = "";
        $pid = "";
        $orderNo = $order['log_id'];
        $howmoney = $order['order_amount'];
        $orderAmount = $howmoney * 100;
        $orderDatetime = date('Ymdhis');
        $orderCurrency = "0";
        $orderExpireDatetime = "";
        $productName = "现金充值";
        $productId = $order['order_sn'];
        $productPrice = "";
        $productNum = "1";
        $productDescription = "";
        $ext1 = "allinpay";
        $ext2 = "";
        $payType = "0"; //payType   不能为空，必须放在表单中提交。
        $issuerId = ""; //issueId 直联时不为空，必须放在表单中提交。
        $pan = "";
        $key = $payment[allinpay_key];

        $bufSignSrc = "";
        if ($inputCharset != "")
            $bufSignSrc = $bufSignSrc . "inputCharset=" . $inputCharset . "&";
        if ($pickupUrl != "")
            $bufSignSrc = $bufSignSrc . "pickupUrl=" . $pickupUrl . "&";
        if ($receiveUrl != "")
            $bufSignSrc = $bufSignSrc . "receiveUrl=" . $receiveUrl . "&";
        if ($version != "")
            $bufSignSrc = $bufSignSrc . "version=" . $version . "&";
        if ($language != "")
            $bufSignSrc = $bufSignSrc . "language=" . $language . "&";
        if ($signType != "")
            $bufSignSrc = $bufSignSrc . "signType=" . $signType . "&";
        if ($merchantId != "")
            $bufSignSrc = $bufSignSrc . "merchantId=" . $merchantId . "&";
        if ($payerName != "")
            $bufSignSrc = $bufSignSrc . "payerName=" . $payerName . "&";
        if ($payerEmail != "")
            $bufSignSrc = $bufSignSrc . "payerEmail=" . $payerEmail . "&";
        if ($payerTelephone != "")
            $bufSignSrc = $bufSignSrc . "payerTelephone=" . $payerTelephone . "&";
        if ($payerIDCard != "")
            $bufSignSrc = $bufSignSrc . "payerIDCard=" . $payerIDCard . "&";
        if ($pid != "")
            $bufSignSrc = $bufSignSrc . "pid=" . $pid . "&";
        if ($orderNo != "")
            $bufSignSrc = $bufSignSrc . "orderNo=" . $orderNo . "&";
        if ($orderAmount != "")
            $bufSignSrc = $bufSignSrc . "orderAmount=" . $orderAmount . "&";
        if ($orderCurrency != "")
            $bufSignSrc = $bufSignSrc . "orderCurrency=" . $orderCurrency . "&";
        if ($orderDatetime != "")
            $bufSignSrc = $bufSignSrc . "orderDatetime=" . $orderDatetime . "&";
        if ($orderExpireDatetime != "")
            $bufSignSrc = $bufSignSrc . "orderExpireDatetime=" . $orderExpireDatetime . "&";
        if ($productName != "")
            $bufSignSrc = $bufSignSrc . "productName=" . $productName . "&";
        if ($productPrice != "")
            $bufSignSrc = $bufSignSrc . "productPrice=" . $productPrice . "&";
        if ($productNum != "")
            $bufSignSrc = $bufSignSrc . "productNum=" . $productNum . "&";
        if ($productId != "")
            $bufSignSrc = $bufSignSrc . "productId=" . $productId . "&";
        if ($productDescription != "")
            $bufSignSrc = $bufSignSrc . "productDescription=" . $productDescription . "&";
        if ($ext1 != "")
            $bufSignSrc = $bufSignSrc . "ext1=" . $ext1 . "&";
        if ($ext2 != "")
            $bufSignSrc = $bufSignSrc . "ext2=" . $ext2 . "&";
        if ($payType != "")
            $bufSignSrc = $bufSignSrc . "payType=" . $payType . "&";
        if ($issuerId != "")
            $bufSignSrc = $bufSignSrc . "issuerId=" . $issuerId . "&";
        if ($pan != "")
            $bufSignSrc = $bufSignSrc . "pan=" . $pan . "&";
        $bufSignSrc = $bufSignSrc . "key=" . $key; //key为MD5密钥，密钥是在通联支付网关会员服务网站上设置。

        $signMsg = strtoupper(md5($bufSignSrc));
        include_once ("allinpay/allinpay.php");
        exit();
    }

    /**
     * 响应操作
     */
    function respond()
    {
        include_once ("allinpay/php_rsa.php");
        include_once ("allinpay/php_sax.php");
        $merchantId = $_POST["merchantId"];
        $version = $_POST['version'];
        $language = $_POST['language'];
        $signType = $_POST['signType'];
        $payType = $_POST['payType'];
        $issuerId = $_POST['issuerId'];
        $paymentOrderId = $_POST['paymentOrderId'];
        $orderNo = $_POST['orderNo'];
        $orderDatetime = $_POST['orderDatetime'];
        $orderAmount = $_POST['orderAmount'];
        $payDatetime = $_POST['payDatetime'];
        $payAmount = $_POST['payAmount'];
        $ext1 = $_POST['ext1'];
        $ext2 = $_POST['ext2'];
        $payResult = $_POST['payResult'];
        $errorCode = $_POST['errorCode'];
        $returnDatetime = $_POST['returnDatetime'];
        $signMsg = $_POST["signMsg"];
        $bufSignSrc = "";
        if ($merchantId != "")
            $bufSignSrc = $bufSignSrc . "merchantId=" . $merchantId . "&";
        if ($version != "")
            $bufSignSrc = $bufSignSrc . "version=" . $version . "&";
        if ($language != "")
            $bufSignSrc = $bufSignSrc . "language=" . $language . "&";
        if ($signType != "")
            $bufSignSrc = $bufSignSrc . "signType=" . $signType . "&";
        if ($payType != "")
            $bufSignSrc = $bufSignSrc . "payType=" . $payType . "&";
        if ($issuerId != "")
            $bufSignSrc = $bufSignSrc . "issuerId=" . $issuerId . "&";
        if ($paymentOrderId != "")
            $bufSignSrc = $bufSignSrc . "paymentOrderId=" . $paymentOrderId . "&";
        if ($orderNo != "")
            $bufSignSrc = $bufSignSrc . "orderNo=" . $orderNo . "&";
        if ($orderDatetime != "")
            $bufSignSrc = $bufSignSrc . "orderDatetime=" . $orderDatetime . "&";
        if ($orderAmount != "")
            $bufSignSrc = $bufSignSrc . "orderAmount=" . $orderAmount . "&";
        if ($payDatetime != "")
            $bufSignSrc = $bufSignSrc . "payDatetime=" . $payDatetime . "&";
        if ($payAmount != "")
            $bufSignSrc = $bufSignSrc . "payAmount=" . $payAmount . "&";
        if ($ext1 != "")
            $bufSignSrc = $bufSignSrc . "ext1=" . $ext1 . "&";
        if ($ext2 != "")
            $bufSignSrc = $bufSignSrc . "ext2=" . $ext2 . "&";
        if ($payResult != "")
            $bufSignSrc = $bufSignSrc . "payResult=" . $payResult . "&";
        if ($errorCode != "")
            $bufSignSrc = $bufSignSrc . "errorCode=" . $errorCode . "&";
        if ($returnDatetime != "")
            $bufSignSrc = $bufSignSrc . "returnDatetime=" . $returnDatetime;
        $key = initPublicKey("allinpay/publickey.xml");
        $keylength = 1024;
        $verify_result = rsa_verify($bufSignSrc, $signMsg, $key[1][0], $key[2][0], $keylength, "sha1");

        $value = null;
        if ($verify_result) {
            $value = "victory";
        } else {
            $value = "failed";
        }
        $payvalue = null;
        $pay_result = false;
        if ($verify_result and $payResult == 1) {
            $pay_result = true;
            $payvalue = "victory";
        } else {
            $payvalue = "failed";
        }

        if ($pay_result) {
            order_paid($orderNo, 2);
            return true;
        } else {
            return false;
        }
    }
}

?>